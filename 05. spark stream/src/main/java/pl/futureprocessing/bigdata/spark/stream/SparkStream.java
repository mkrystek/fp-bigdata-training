package pl.futureprocessing.bigdata.spark.stream;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;

public class SparkStream {
    private static final String HOST = "localhost";
    private static final int PORT = 9999;

    private static class SplittingFunction implements FlatMapFunction<String, String> {

        @Override
        public Iterable<String> call(String line) throws Exception {
            return null;
        }
    }

    private static class MappingFunction implements PairFunction<String, String, Integer> {

        @Override
        public Tuple2<String, Integer> call(String word) throws Exception {
            return null;
        }
    }

    private static class ReducingFunction implements Function2<Integer, Integer, Integer> {

        @Override
        public Integer call(Integer v1, Integer v2) throws Exception {
            return v1 + v2;
        }
    }

    public static void main(String[] args) {

        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("SparkStreamApp");

        JavaStreamingContext streamingContext =
                new JavaStreamingContext(conf, Durations.seconds(5));
        Logger.getRootLogger().setLevel(Level.ERROR);

        JavaReceiverInputDStream<String> lines = streamingContext.socketTextStream(HOST, PORT);
        JavaDStream<String> words = lines.flatMap(new SplittingFunction());
        JavaPairDStream<String, Integer> pairs = words.mapToPair(new MappingFunction());

        JavaPairDStream<String, Integer> wordCountsInWindow =
                pairs.reduceByKeyAndWindow(new ReducingFunction(), Durations.seconds(20), Durations.seconds(10));

        wordCountsInWindow.print();

        streamingContext.start();
        streamingContext.awaitTermination();
    }
}
