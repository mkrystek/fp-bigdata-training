package pl.futureprocessing.bigdata.mapreduce.reddit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class RedditScore extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new RedditScore(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            printErrorMessages();
            return 1;
        }

        Job job = Job.getInstance(getConf(), "RedditScore");
        job.setJarByClass(getClass());

        job.setMapperClass(RedditScoreMapper.class);
        job.setCombinerClass(RedditScoreReducer.class);
        job.setReducerClass(RedditScoreReducer.class);

        job.setOutputKeyClass(SubredditUserWritable.class);
        job.setOutputValueClass(LongWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    private static void printErrorMessages() {
        System.err.println("Usage: hadoop jar reddit-mapreduce-1.0-job.jar" + " [generic options] <in> <out>");
        System.out.println();
        ToolRunner.printGenericCommandUsage(System.err);
    }
}