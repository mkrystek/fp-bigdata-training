package pl.futureprocessing.bigdata.mapreduce.reddit;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import com.google.common.collect.ComparisonChain;
import org.apache.hadoop.io.WritableComparable;

public class SubredditUserWritable implements WritableComparable<SubredditUserWritable> {

    private String subreddit;
    private String author;

    public SubredditUserWritable() {

    }

    public SubredditUserWritable(String subreddit, String author) {
        this.subreddit = subreddit;
        this.author = author;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        subreddit = in.readUTF();
        author = in.readUTF();
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(subreddit);
        out.writeUTF(author);
    }

    @Override
    public int compareTo(SubredditUserWritable o) {
        return ComparisonChain.start().compare(subreddit, o.subreddit).compare(author, o.author).result();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubredditUserWritable that = (SubredditUserWritable) o;

        if (subreddit != null ? !subreddit.equals(that.subreddit) : that.subreddit != null) return false;
        return !(author != null ? !author.equals(that.author) : that.author != null);
    }

    @Override
    public int hashCode() {
        int result = subreddit != null ? subreddit.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return subreddit + " " + author;
    }
}
