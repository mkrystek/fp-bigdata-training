package pl.futureprocessing.bigdata.mapreduce.reddit;

import java.io.IOException;
import com.google.gson.Gson;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class RedditScoreMapper extends Mapper<LongWritable, Text, SubredditUserWritable, LongWritable> {

    @Override
    public void map(LongWritable offset, Text value, Context context) throws IOException, InterruptedException {
        RedditPost redditPost = parsePost(value);

        if (postIsValid(redditPost)) {
            //store post data in context
        }
    }

    private RedditPost parsePost(Text value) {
        Gson gson = new Gson();
        String post = value.toString();
        return gson.fromJson(post, RedditPost.class);
    }

    private boolean postIsValid(RedditPost post) {
        //check if post contains all the data we need
        return false;
    }
}
