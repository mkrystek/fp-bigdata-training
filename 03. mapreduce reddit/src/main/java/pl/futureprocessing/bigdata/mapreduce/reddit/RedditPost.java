package pl.futureprocessing.bigdata.mapreduce.reddit;

public class RedditPost {

    private String subreddit;
    private String author;
    private Long score;

    public Long getScore() {
        return score;
    }

    public String getSubreddit() {
        return subreddit;
    }

    public String getAuthor() {
        return author;
    }
}
