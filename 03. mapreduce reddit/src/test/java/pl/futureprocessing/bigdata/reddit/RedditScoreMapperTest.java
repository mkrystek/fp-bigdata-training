package pl.futureprocessing.bigdata.reddit;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.futureprocessing.bigdata.mapreduce.reddit.RedditScoreMapper;
import pl.futureprocessing.bigdata.mapreduce.reddit.SubredditUserWritable;

@RunWith(MockitoJUnitRunner.class)
public class RedditScoreMapperTest {

    @Mock
    private Mapper<LongWritable, Text, SubredditUserWritable, LongWritable>.Context context;

    private RedditScoreMapper mapper;

    @Before
    public void setUp() {
        mapper = new RedditScoreMapper();
    }

    @Test
    public void shouldHandlePostWithLowScore() throws IOException, InterruptedException {
        Text input = new Text(
            "{\"controversiality\":0,\"created_utc\":\"1243814400\",\"subreddit\":\"wacrover\",\"author_flair_css_class\":null,\"subreddit_id\":\"t5_2qxnb\",\"id\":\"c09yi51\",\"name\":\"t1_c09yi51\",\"gilded\":0,\"parent_id\":\"t1_c09sxo0\",\"ups\":1,\"author\":\"wacrover\",\"link_id\":\"t3_8mxco\",\"edited\":false,\"author_flair_text\":null,\"body\":\"I'll miss you.\",\"distinguished\":null,\"retrieved_on\":1425978963,\"archived\":true,\"downs\":0,\"score_hidden\":false,\"score\":1}");

        mapper.map(new LongWritable(0), input, context);
        verify(context, times(1)).write(new SubredditUserWritable("wacrover", "wacrover"), new LongWritable(1));
        verifyNoMoreInteractions(context);
    }

    @Test
    public void shouldHandlePostWithHighScore() throws IOException, InterruptedException {
        Text input = new Text(
            "{\"retrieved_on\":1425986748,\"score\":2960,\"gilded\":1,\"subreddit_id\":\"t5_6\",\"name\":\"t1_c0aebis\",\"subreddit\":\"reddit.com\",\"body\":\"The movie was that bad?\",\"downs\":0,\"controversiality\":0,\"distinguished\":null,\"author\":\"AnnArchist\",\"parent_id\":\"t3_8tpx3\",\"id\":\"c0aebis\",\"created_utc\":\"1245362872\",\"ups\":2960,\"score_hidden\":false,\"edited\":false,\"link_id\":\"t3_8tpx3\",\"author_flair_text\":null,\"archived\":true,\"author_flair_css_class\":null}");

        mapper.map(new LongWritable(0), input, context);
        verify(context, times(1)).write(new SubredditUserWritable("reddit.com", "AnnArchist"), new LongWritable(2960));
        verifyNoMoreInteractions(context);
    }

    @Test
    public void shouldIgnorePostWithoutScore() throws IOException, InterruptedException {
        Text input = new Text(
            "{\"retrieved_on\":1425986748,\"gilded\":1,\"subreddit_id\":\"t5_6\",\"name\":\"t1_c0aebis\",\"subreddit\":\"reddit.com\",\"body\":\"The movie was that bad?\",\"downs\":0,\"controversiality\":0,\"distinguished\":null,\"author\":\"AnnArchist\",\"parent_id\":\"t3_8tpx3\",\"id\":\"c0aebis\",\"created_utc\":\"1245362872\",\"ups\":2960,\"score_hidden\":false,\"edited\":false,\"link_id\":\"t3_8tpx3\",\"author_flair_text\":null,\"archived\":true,\"author_flair_css_class\":null}");

        mapper.map(new LongWritable(0), input, context);
        verifyNoMoreInteractions(context);
    }

    @Test
    public void shouldIgnorePostWithoutSubreddit() throws IOException, InterruptedException {
        Text input = new Text(
            "{\"controversiality\":0,\"created_utc\":\"1243814400\",\"author_flair_css_class\":null,\"subreddit_id\":\"t5_2qxnb\",\"id\":\"c09yi51\",\"name\":\"t1_c09yi51\",\"gilded\":0,\"parent_id\":\"t1_c09sxo0\",\"ups\":1,\"author\":\"wacrover\",\"link_id\":\"t3_8mxco\",\"edited\":false,\"author_flair_text\":null,\"body\":\"I'll miss you.\",\"distinguished\":null,\"retrieved_on\":1425978963,\"archived\":true,\"downs\":0,\"score_hidden\":false,\"score\":1}");

        mapper.map(new LongWritable(0), input, context);
        verifyNoMoreInteractions(context);
    }
    @Test
    public void shouldIgnorePostWithoutAuthor() throws IOException, InterruptedException {
        Text input = new Text(
            "{\"retrieved_on\":1425986748,\"score\":2960,\"gilded\":1,\"subreddit_id\":\"t5_6\",\"name\":\"t1_c0aebis\",\"subreddit\":\"reddit.com\",\"body\":\"The movie was that bad?\",\"downs\":0,\"controversiality\":0,\"distinguished\":null,\"parent_id\":\"t3_8tpx3\",\"id\":\"c0aebis\",\"created_utc\":\"1245362872\",\"ups\":2960,\"score_hidden\":false,\"edited\":false,\"link_id\":\"t3_8tpx3\",\"author_flair_text\":null,\"archived\":true,\"author_flair_css_class\":null}");

        mapper.map(new LongWritable(0), input, context);
        verifyNoMoreInteractions(context);
    }
}