package pl.futureprocessing.bigdata.reddit;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.futureprocessing.bigdata.mapreduce.reddit.RedditScoreReducer;
import pl.futureprocessing.bigdata.mapreduce.reddit.SubredditUserWritable;

@RunWith(MockitoJUnitRunner.class)
public class RedditScoreReducerTest {

    @Mock
    private Reducer<SubredditUserWritable, LongWritable, SubredditUserWritable, LongWritable>.Context context;

    private RedditScoreReducer reducer;

    @Before
    public void setUp() {
        reducer = new RedditScoreReducer();
    }

    @Test
    public void testBasicReducerBehaviour() throws IOException, InterruptedException {
        List<LongWritable> mapValues = new ArrayList<>();
        mapValues.add(new LongWritable(1));
        mapValues.add(new LongWritable(0));
        mapValues.add(new LongWritable(1));
        mapValues.add(new LongWritable(2));
        SubredditUserWritable key = new SubredditUserWritable("reddit.com", "redditUser");

        reducer.reduce(key, mapValues, context);

        verify(context, times(1)).write(key, new LongWritable(4));
        verifyNoMoreInteractions(context);
    }
}
