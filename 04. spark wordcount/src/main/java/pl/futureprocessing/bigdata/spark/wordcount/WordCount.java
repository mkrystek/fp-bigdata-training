package pl.futureprocessing.bigdata.spark.wordcount;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

public class WordCount {
    private static class SplittingFunction implements FlatMapFunction<String, String> {

        @Override
        public Iterable<String> call(String line) throws Exception {
            // TODO split line into tokens and return list of words
            return null;
        }
    }

    private static class MappingFunction implements PairFunction<String, String, Integer> {

        @Override
        public Tuple2<String, Integer> call(String word) throws Exception {
            // TODO for each word return tuple of <word, 1>
            return null;
        }
    }

    private static class ReducingFunction implements Function2<Integer, Integer, Integer> {

        @Override
        public Integer call(Integer v1, Integer v2) throws Exception {
            return v1 + v2;
        }
    }

    public static void main(String[] args) throws Exception {
        String inputFile = args[0];
        String outputFile = args[1];

        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("SparkWordcountApp");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> input = sc.textFile(inputFile);

        JavaRDD<String> words = input.flatMap(new SplittingFunction());

        JavaPairRDD<String, Integer> counts = words
                .mapToPair(new MappingFunction())
                .reduceByKey(new ReducingFunction());

        counts.saveAsTextFile(outputFile);
        sc.close();
    }
}
